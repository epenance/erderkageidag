import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthService {
  private user: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  public loggedIn$ = new BehaviorSubject(false);

  public showForm$: BehaviorSubject<null | 'login' | 'signup'> = new BehaviorSubject(null);

  constructor(private firebaseAuth: AngularFireAuth) {
    this.user = firebaseAuth.authState;

    this.user.subscribe((user) => {
      if (user) {
        this.userDetails = user;
      } else {
        this.userDetails = null;
      }

      this.loggedIn$.next(this.isLoggedIn());
    });
  }

  createUserByEmail(email, password) {
    return this.firebaseAuth.auth.createUserAndRetrieveDataWithEmailAndPassword(email, password);
  }

  signInByEmail(email, password) {
    return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);
  }

  isLoggedIn() {
    return this.userDetails != null;
  }

  logout() {
    return this.firebaseAuth.auth.signOut();
  }

  showLoginForm() {
    this.showForm$.next('login');
  }

  showSignupForm() {
    this.showForm$.next('signup');
  }

  hideForm() {
    this.showForm$.next(null);
  }

}
