import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import { AppAuthModule } from '@features/auth/auth.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { CoreModule } from '@core/core.module';
import { CakeModule } from '@features/cake/cake.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    AngularFireModule.initializeApp(environment.firebase, 'er-der-kage-idag'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    CoreModule,
    AppAuthModule,
    CakeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
