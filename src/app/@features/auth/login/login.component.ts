import { Component, OnInit } from '@angular/core';
import { AuthService } from '@core/auth.service';
import { SignUpRequest } from '@features/auth/signup.class';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user = new SignUpRequest('', '');
  isLoggedIn$ = this.auth.loggedIn$;

  constructor(private auth: AuthService) { }

  ngOnInit() {
  }

  signIn(form) {
    if (form.valid) {
      this.auth.signInByEmail(this.user.email, this.user.password)
        .then((res) => {
          console.log(res);
          this.auth.hideForm();
        })
        .catch((err) => console.log('error' + err));
    }
  }

  logout() {
    this.auth.logout().then(() => {
      this.auth.showLoginForm();
    });
  }

  showSignupForm() {
    this.auth.showSignupForm();
  }

  hideForm() {
    this.auth.hideForm();
  }

}
