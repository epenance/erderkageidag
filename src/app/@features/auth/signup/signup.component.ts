import { Component, OnInit } from '@angular/core';
import { AuthService } from '@core/auth.service';
import { SignUpRequest } from '@features/auth/signup.class';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  user = new SignUpRequest('', '');
  isLoggedIn$ = this.auth.loggedIn$;

  constructor(private auth: AuthService) { }

  ngOnInit() {
  }

  createUser(form) {
    if (form.valid) {
      this.auth.createUserByEmail(this.user.email, this.user.password)
        .then((res) => {
          console.log(res);
          this.auth.hideForm();
        })
        .catch((err) => console.log('error' + err));
    }
  }

  logout() {
    this.auth.logout().then(() => {
      this.auth.showLoginForm();
    });
  }

  showLoginForm() {
    this.auth.showLoginForm();
  }

  hideForm() {
    this.auth.hideForm();
  }

}
