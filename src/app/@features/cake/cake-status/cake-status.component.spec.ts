import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CakeStatusComponent } from './cake-status.component';

describe('CakeStatusComponent', () => {
  let component: CakeStatusComponent;
  let fixture: ComponentFixture<CakeStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CakeStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CakeStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
