import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { map, share, tap } from 'rxjs/operators';

@Component({
  selector: 'app-cake-status',
  templateUrl: './cake-status.component.html',
  styleUrls: ['./cake-status.component.scss']
})
export class CakeStatusComponent implements OnInit {
  cakeRef: AngularFireObject<any>;
  thereIsCake: Observable<any>;

  @ViewChild('yay') private yaySound;
  @ViewChild('noo') private nooSound;

  constructor(private db: AngularFireDatabase) {
    this.cakeRef = db.object('isCakeToday');
    this.thereIsCake = this.cakeRef.valueChanges().pipe(tap((res) => {
      this.resetSounds();

      if (res) {
        this.yaySound.nativeElement.play();
      } else {
        this.nooSound.nativeElement.play();
      }
    }), share());
  }

  ngOnInit() {

  }

  resetSounds() {
    this.yaySound.nativeElement.pause();
    this.yaySound.nativeElement.currentTime = 0;
    this.nooSound.nativeElement.pause();
    this.nooSound.nativeElement.currentTime = 0;
  }

}
