import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CakeToggleComponent } from './cake-toggle.component';

describe('CakeToggleComponent', () => {
  let component: CakeToggleComponent;
  let fixture: ComponentFixture<CakeToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CakeToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CakeToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
