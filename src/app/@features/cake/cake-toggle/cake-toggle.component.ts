import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-cake-toggle',
  templateUrl: './cake-toggle.component.html',
  styleUrls: ['./cake-toggle.component.scss']
})
export class CakeToggleComponent implements OnInit {
  cakeRef: AngularFireObject<any>;
  thereIsCake: Observable<any>;

  constructor(private db: AngularFireDatabase) {
    this.cakeRef = db.object('isCakeToday');
    this.thereIsCake = this.cakeRef.valueChanges();
  }

  ngOnInit() {
  }

  changeCakeStatus(status: boolean) {
    this.cakeRef.set(status).catch(() => {
      console.warn('You don\'t have the rights to change the cake status');
    });
  }

}
