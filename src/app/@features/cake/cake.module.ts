import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CakeToggleComponent } from './cake-toggle/cake-toggle.component';
import { FormsModule } from '@angular/forms';
import { CakeStatusComponent } from './cake-status/cake-status.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [CakeToggleComponent, CakeStatusComponent],
  exports: [CakeToggleComponent, CakeStatusComponent]
})
export class CakeModule { }
