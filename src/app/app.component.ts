import { Component } from '@angular/core';
import { AuthService } from '@core/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  showForm$ = this.auth.showForm$;
  isLoggedIn$ = this.auth.loggedIn$;

  constructor(private auth: AuthService) {

  }

  showLogin() {
    this.auth.showLoginForm();
  }
}
