// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyB6Jgi5xgm0zCUHfFrY5Bho0zkH1GI5P9Y',
    authDomain: 'er-der-kage-idag.firebaseapp.com',
    databaseURL: 'https://er-der-kage-idag.firebaseio.com',
    projectId: 'er-der-kage-idag',
    storageBucket: 'er-der-kage-idag.appspot.com',
    messagingSenderId: '225503661266'
  }
};
